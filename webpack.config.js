const path = require('path');

module.exports = {
    mode: "production",
    entry: ["./src/index.js"],
    output: {
        path: path.resolve(__dirname, "public/assets/js"),
        filename: "tmdocs.js",
        publicPath: "https://tanuel.gitlab.io/assets/js/"
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            },{
                test: /\.scss$/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS, using Node Sass by default
                ]
            },
        ],
    },
};