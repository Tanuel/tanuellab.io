//Highlightjs
import "highlight.js/styles/vs2015.css";
import "./scss/main.scss";

import(/* webpackChunkName: "hljs" */ 'highlight.js/lib/highlight').then(hljs => {
    const languages = ["javascript", "typescript", "xml", "css", "scss"];
    languages.forEach(language => {
       import(/* webpackChunkName: "hljs-languages" */ 'highlight.js/lib/languages/' + language).then(({default: langHL}) => {
          hljs.registerLanguage(language, langHL);
       });
    });
    window.hljs = hljs;
    hljs.initHighlightingOnLoad();
});
